package revolut.task.transferapp;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Executors;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import revolut.task.transferapp.account.command.CreateAccountCommand;
import revolut.task.transferapp.account.domain.Currency;
import revolut.task.transferapp.account.query.AccountProjection;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.command.GuavaCommandBus;
import revolut.task.transferapp.domain.event.DomainEventBus;
import revolut.task.transferapp.infra.GuavaDomainEventBus;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.transfer.application.command.BeginTransferCommand;
import revolut.task.transferapp.transfer.domain.TransferState;
import revolut.task.transferapp.transfer.query.TransferProjection;

public class TransferApplicationAcceptanceTest {
    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // command bus
        CommandBus sharedCommandBus = GuavaCommandBus.syncGuavaCommandBus("shared-command-bus");

        // domain event bus
        DomainEventBus domainEventBus = GuavaDomainEventBus.asyncGuavaDomainEventBus("domain-event-bus",
                Executors.newFixedThreadPool(1));

        server = TransferApplication.prepareServer(sharedCommandBus, domainEventBus );
        server.start();
        Client c = ClientBuilder.newClient();
        target = c.target(TransferApplication.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(1_000);
        server.shutdownNow();
    }

    @Test
    public void test_account_creation() throws Exception {
        // Given
        String ownerIdNumber = "123456790ID";
        String ownerFirstName = "Jan";
        String ownerLastName = "Nowak";
        BigDecimal accountStartBalance = BigDecimal.valueOf(1_000);
        Currency currency = Currency.PLN;

        // Verify if there is no account for given ownerIdNumber
        List<AccountProjection> accountsForGivenOwnerId = target.path("accounts").queryParam("ownerIdNumber", ownerIdNumber)
                .request().get(new GenericType<List<AccountProjection>>() {});
        assertThat(accountsForGivenOwnerId).isEmpty();

        // Prepare request
        CreateAccountCommand command = new CreateAccountCommand();
        command.setAccountCurrency(Currency.PLN);
        command.setAccountStartBalance(accountStartBalance);
        command.setOwnerFirstName(ownerFirstName);
        command.setOwnerLastName(ownerLastName);
        command.setOwnerIdNumber(ownerIdNumber);
        Entity<CreateAccountCommand> requestEntity = Entity.entity(command, MediaType.APPLICATION_JSON_TYPE);

        // Create account
        Response response = target.path("accounts").request().put(requestEntity);

        // Verify response code
        assertThat(response.getStatus()).isEqualTo(200);

        // List accounts for given ownerIdNumber
        accountsForGivenOwnerId = target.path("accounts").queryParam("ownerIdNumber", ownerIdNumber)
                .request().get(new GenericType<List<AccountProjection>>() {});

        // There should be one account
        assertThat(accountsForGivenOwnerId).hasSize(1);

        // With data same as given
        AccountProjection createdAccount = accountsForGivenOwnerId.get(0);
        assertThat(createdAccount.getBalance()).isEqualTo(accountStartBalance);
        assertThat(createdAccount.getAccountOwner().getIdNumber()).isEqualTo(ownerIdNumber);
        assertThat(createdAccount.getAccountOwner().getFirstName()).isEqualTo(ownerFirstName);
        assertThat(createdAccount.getAccountOwner().getLastName()).isEqualTo(ownerLastName);
        assertThat(createdAccount.getCurrency()).isEqualTo(currency);
    }

    @Test
    public void positive_transfer_test() throws Exception {
        // const
        BigDecimal transferAmount = BigDecimal.valueOf(100);
        String transferTitle = "Test transfer";

        // Create accounts
        AccountId sourceAccountId = createAccountAndGetId(BigDecimal.valueOf(1_000), "Jan", "Nowak", "123456790ID");
        AccountId targetAccountId = createAccountAndGetId(BigDecimal.ZERO, "Jan", "Kowalski", "0987654321ID");

        // Prepare request
        BeginTransferCommand command = new BeginTransferCommand();
        command.setAmount(transferAmount);
        command.setSourceAccountId(sourceAccountId);
        command.setTargetAccountId(targetAccountId);
        command.setTitle(transferTitle);
        Entity<BeginTransferCommand> requestEntity = Entity.entity(command, MediaType.APPLICATION_JSON_TYPE);

        // Begin transfer
        Response response = target.path("transfers").request().put(requestEntity);
        System.out.println(response);

        // Verify response code
        assertThat(response.getStatus()).isEqualTo(200);

        // Wait for async
        Thread.sleep(1_000);

        // Verify source account balance
        AccountProjection sourceAccount =  target.path("accounts/"+sourceAccountId).request().get(AccountProjection.class);
        assertThat(sourceAccount.getBalance()).isEqualTo(BigDecimal.valueOf(900));

        // Verify source account history
        List<TransferProjection> sourceAccountTransferHistory = target.path("transfers").queryParam("accountId", sourceAccountId.getId())
                .request().get(new GenericType<List<TransferProjection>>(){});
        assertThat(sourceAccountTransferHistory).hasSize(1);
        TransferProjection sourceAccountLatestTransfer = sourceAccountTransferHistory.get(0);
        assertThat(sourceAccountLatestTransfer.getAmount()).isEqualTo(transferAmount);
        assertThat(sourceAccountLatestTransfer.getTitle()).isEqualTo(transferTitle);
        assertThat(sourceAccountLatestTransfer.getFromAccount()).isEqualTo(sourceAccountId);
        assertThat(sourceAccountLatestTransfer.getToAccount()).isEqualTo(targetAccountId);
        assertThat(sourceAccountLatestTransfer.getState()).isEqualTo(TransferState.FINISHED);
        assertThat(sourceAccountLatestTransfer.getFailureReason()).isNull();

        // Verify target account balance
        AccountProjection targetAccount =  target.path("accounts/"+targetAccountId).request().get(AccountProjection.class);
        assertThat(targetAccount.getBalance()).isEqualTo(BigDecimal.valueOf(100));

        // Verify target account history
        List<TransferProjection> targetAccountTransferHistory = target.path("transfers").queryParam("accountId", targetAccountId.getId())
                .request().get(new GenericType<List<TransferProjection>>(){});
        assertThat(targetAccountTransferHistory).hasSize(1);
        TransferProjection targetAccountLatestTransfer = sourceAccountTransferHistory.get(0);
        assertThat(targetAccountLatestTransfer).isEqualTo(sourceAccountLatestTransfer);
    }

    @Test
    public void negative_transfer_test() throws Exception {
        // const
        BigDecimal transferAmount = BigDecimal.valueOf(100);
        String transferTitle = "Test transfer";

        // Create accounts
        AccountId targetAccountId = createAccountAndGetId(BigDecimal.valueOf(1_000), "Jan", "Nowak", "123456790ID");
        AccountId sourceAccountId = createAccountAndGetId(BigDecimal.ZERO, "Jan", "Kowalski", "0987654321ID");

        // Prepare request
        BeginTransferCommand command = new BeginTransferCommand();
        command.setAmount(transferAmount);
        command.setSourceAccountId(sourceAccountId);
        command.setTargetAccountId(targetAccountId);
        command.setTitle(transferTitle);
        Entity<BeginTransferCommand> requestEntity = Entity.entity(command, MediaType.APPLICATION_JSON_TYPE);

        // Begin transfer
        Response response = target.path("transfers").request().put(requestEntity);

        // Verify response code
        assertThat(response.getStatus()).isEqualTo(200);

        // Wait for async
        Thread.sleep(1_000); // async

        // Verify source account balance
        AccountProjection sourceAccount =  target.path("accounts/"+sourceAccountId).request().get(AccountProjection.class);
        assertThat(sourceAccount.getBalance()).isEqualTo(BigDecimal.ZERO);

        // Verify source account history
        List<TransferProjection> sourceAccountTransferHistory = target.path("transfers").queryParam("accountId", sourceAccountId.getId())
                .request().get(new GenericType<List<TransferProjection>>(){});
        assertThat(sourceAccountTransferHistory).hasSize(1);
        TransferProjection sourceAccountLatestTransfer = sourceAccountTransferHistory.get(0);
        assertThat(sourceAccountLatestTransfer.getAmount()).isEqualTo(transferAmount);
        assertThat(sourceAccountLatestTransfer.getTitle()).isEqualTo(transferTitle);
        assertThat(sourceAccountLatestTransfer.getFromAccount()).isEqualTo(sourceAccountId);
        assertThat(sourceAccountLatestTransfer.getToAccount()).isEqualTo(targetAccountId);
        assertThat(sourceAccountLatestTransfer.getState()).isEqualTo(TransferState.FAILED);
        assertThat(sourceAccountLatestTransfer.getFailureReason()).isNotEmpty();

        // Verify target account balance
        AccountProjection targetAccount =  target.path("accounts/"+targetAccountId).request().get(AccountProjection.class);
        assertThat(targetAccount.getBalance()).isEqualTo(BigDecimal.valueOf(1_000));

        // Verify target account history
        List<TransferProjection> targetAccountTransferHistory = target.path("transfers").queryParam("accountId", targetAccountId.getId())
                .request().get(new GenericType<List<TransferProjection>>(){});
        assertThat(targetAccountTransferHistory).hasSize(0); // There should be no info in target account history about failed transfer
    }

    private AccountId createAccountAndGetId(BigDecimal balance, String ownerFirstName, String ownerLastName, String ownerIdNumber) {
        CreateAccountCommand command = new CreateAccountCommand();
        command.setAccountCurrency(Currency.PLN);
        command.setAccountStartBalance(balance);
        command.setOwnerFirstName(ownerFirstName);
        command.setOwnerLastName(ownerLastName);
        command.setOwnerIdNumber(ownerIdNumber);
        Entity<CreateAccountCommand> requestEntity = Entity.entity(command, MediaType.APPLICATION_JSON_TYPE);

        target.path("accounts").request().put(requestEntity);

        List<AccountProjection> accountsForGivenOwnerId = target.path("accounts").queryParam("ownerIdNumber", ownerIdNumber)
                .request().get(new GenericType<List<AccountProjection>>() {});

        assertThat(accountsForGivenOwnerId).hasSize(1);

        return accountsForGivenOwnerId.get(0).getId();
    }

}
