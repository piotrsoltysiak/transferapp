package revolut.task.transferapp.infra;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.domain.AggregateRoot;
import revolut.task.transferapp.domain.GenericId;
import revolut.task.transferapp.domain.Repository;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.domain.event.DomainEventBus;
import revolut.task.transferapp.domain.event.DomainEventStore;

import java.util.List;

import static java.lang.String.format;

@RequiredArgsConstructor
@Slf4j
public class DefaultRepository implements Repository {
    private final DomainEventBus domainEventBus;
    private final DomainEventStore domainEventStore;

    @Override
    public <AR extends AggregateRoot> void save(AR aggregateRoot) {
        if (aggregateRoot.hasUncommittedEvents()) {
            List<DomainEvent> newEvents = aggregateRoot.getUncommittedEvents();
            domainEventStore.store(aggregateRoot.getId(), aggregateRoot.getClass(), newEvents);
            domainEventBus.publish(newEvents);
        }
    }

    @Override
    public <AR extends AggregateRoot, ID extends GenericId> AR load(ID id, Class<AR> aggregateType) {
        try {
            AR aggregateRoot = aggregateType.newInstance();
            aggregateRoot.loadFromHistory(domainEventStore.loadEventStream(id));
            return aggregateRoot;
        } catch (IllegalArgumentException iae) {
            String message = format("Aggregate of type [%s] does not exist, ID: %s", aggregateType.getSimpleName(), id);
            throw new IllegalArgumentException(message);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
