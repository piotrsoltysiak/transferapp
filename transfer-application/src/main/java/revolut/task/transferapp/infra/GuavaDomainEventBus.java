package revolut.task.transferapp.infra;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.command.GuavaCommandBus;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.domain.event.DomainEventBus;
import revolut.task.transferapp.domain.event.DomainEventListener;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class GuavaDomainEventBus implements DomainEventBus {
    private final EventBus eventBus;

    private GuavaDomainEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public static DomainEventBus asyncGuavaDomainEventBus(String name, Executor executor) {
        return new GuavaDomainEventBus(new AsyncEventBus(name, executor));
    }

    @Override
    public <T extends DomainEventListener> T register(T listener) {
        eventBus.register(listener);
        return listener;
    }

    @Override
    public void publish(List<DomainEvent> events) {
        for (DomainEvent event : events) {
            log.debug("Publishing domain event: {}", event);
            eventBus.post(event);
        }
    }

}
