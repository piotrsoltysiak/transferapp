package revolut.task.transferapp.infra;

import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.domain.event.DomainEventStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InMemoryDomainEventStream implements DomainEventStream {
    private final long version;
    private final List<DomainEvent> stream;

    public InMemoryDomainEventStream(List<DomainEvent> stream) {
        this.stream = stream;
        this.version = 0;
    }

    public long version() {
        return 0;
    }

    public Iterator<DomainEvent> iterator() {
        return stream.iterator();
    }

    public static InMemoryDomainEventStream empty() {
        return new InMemoryDomainEventStream(new ArrayList<>());
    }

    @Override
    public void addAll(List<DomainEvent> changes) {
        stream.addAll(changes);
    }

    @Override
    public long count() {
        return stream.size();
    }
}
