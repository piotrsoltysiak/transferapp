package revolut.task.transferapp.infra;

import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.domain.AggregateRoot;
import revolut.task.transferapp.domain.GenericId;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.domain.event.DomainEventStore;
import revolut.task.transferapp.domain.event.DomainEventStream;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class InMemoryDomainEventStore implements DomainEventStore {

    private Map<GenericId, DomainEventStream> store = new ConcurrentHashMap<>(10_000);

    @Override
    public DomainEventStream loadEventStream(GenericId aggregateId) {
        return store.get(aggregateId);
    }

    @Override
    public void store(GenericId aggregateId, Class<? extends AggregateRoot> type, List<DomainEvent> events) {
        log.debug("Saving aggregate root of type {} with id {} and events {}", type, aggregateId, events);
        store.computeIfAbsent(aggregateId, id -> InMemoryDomainEventStream.empty()).addAll(events);
    }
}
