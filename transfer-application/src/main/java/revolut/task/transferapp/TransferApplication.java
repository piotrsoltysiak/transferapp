package revolut.task.transferapp;

import lombok.extern.slf4j.Slf4j;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import revolut.task.transferapp.account.config.AccountContextConfig;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.command.GuavaCommandBus;
import revolut.task.transferapp.domain.Repository;
import revolut.task.transferapp.domain.event.DomainEventBus;
import revolut.task.transferapp.infra.DefaultRepository;
import revolut.task.transferapp.infra.GuavaDomainEventBus;
import revolut.task.transferapp.infra.InMemoryDomainEventStore;
import revolut.task.transferapp.transfer.config.TransferContextConfig;

import javax.ws.rs.core.Application;
import java.net.URI;
import java.util.concurrent.Executors;

@Slf4j
public class TransferApplication extends Application {
    static final String BASE_URI = "http://localhost:8080/transferapp/";

    static HttpServer prepareServer(CommandBus sharedCommandBus, DomainEventBus domainEventBus) {
        // transaction context config
        Repository transferRepository = new DefaultRepository(domainEventBus, new InMemoryDomainEventStore());
        TransferContextConfig transferContextConfig = new TransferContextConfig(sharedCommandBus, transferRepository);
        domainEventBus.register(transferContextConfig.transferSaga());
        domainEventBus.register(transferContextConfig.transferListDenormalizer());
        sharedCommandBus.register(transferContextConfig.transferCommandHandler());

        // account context config
        Repository accountRepository = new DefaultRepository(domainEventBus, new InMemoryDomainEventStore());
        AccountContextConfig accountContextConfig = new AccountContextConfig(sharedCommandBus, accountRepository);
        domainEventBus.register(accountContextConfig.accountListDenormalizer());
        sharedCommandBus.register(accountContextConfig.accountCommandHandler());

        // Grizzly resource config
        ResourceConfig config = new ResourceConfig();
        config.register(transferContextConfig.transferController());
        config.register(transferContextConfig.transferResource());
        config.register(accountContextConfig.accountController());
        config.register(accountContextConfig.accountResource());


        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), config, false);
    }

    public static void main(String[] args) throws Exception {
        // command bus
        CommandBus sharedCommandBus = GuavaCommandBus.asyncGuavaCommandBus("shared-command-bus",
                Executors.newFixedThreadPool(10));

        // domain event bus
        DomainEventBus domainEventBus = GuavaDomainEventBus.asyncGuavaDomainEventBus("domain-event-bus",
                Executors.newFixedThreadPool(10));

        final HttpServer server = prepareServer(sharedCommandBus, domainEventBus);
        server.start();
        log.info("Transfer app started");
    }


}