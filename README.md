# TransferApp

## Description
TransferApp is multi-module java application based on CQRS/ES paradigm and Domain Driven Design. It's made of two contexts - account and transfer, bounded together with command and domain event bus. 
Application runs on embedded grizzly2 server with RESTful API based on Jersey. Due to "keep it simple" requirement there is no IoC/DI container, and only one production-code library (Guava).
There is also no persistent datastore - only in-memory, hashmap based event store and projection repositories implemented the same way. 

Due to lack of time I wasn't able to write proper API tests in newman/postman/whatewer. Instead of that there is TransferApplicationAcceptanceTest class, which runs the server and perform all test cases with ResAssured. 

## API

### Account context
`PUT /accounts` - command entry point

`GET /accounts` - query entry point, currently only ownerIdNumber query param supported

`GET /accounts/{accountId}` - get account projection for its id


### Transfer context
`PUT /transfers` - command entry point

`GET /transfers` - query entry point, currently only accountId query param supported

`GET /transfers/{transferid}` - get transfer projection for its id


## Requirements
Latest java and maven versions. App will run on port 8080.

## Installation
`mvn clean package`

## Tests
Unit tests:
`mvn test`

Integration and acceptance tests:
`mvn integration-test`

## Execution
`java -jar transfer-application\target\transfer-appilcation-1.0-SNAPSHOT-jar-with-dependencies.jar`

## TODO
- ~~add more acceptance test cases~~
- integration tests
- introduce currencies :)
- pass expected port as argument