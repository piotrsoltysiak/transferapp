package revolut.task.transferapp.shared.event;

import lombok.Builder;
import lombok.Getter;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class AccountDebitedEvent extends DomainEvent<AccountId> {
    private final BigDecimal amount;
    private final AccountId targetAccountId;
    private final String title;
    private final TransferId transferId;

    @Builder
    public AccountDebitedEvent(AccountId aggregateId, int version, LocalDateTime timestamp, BigDecimal amount,
                               AccountId targetAccountId, String title, TransferId transferId) {
        super(aggregateId, version, timestamp);
        this.amount = amount;
        this.targetAccountId = targetAccountId;
        this.title = title;
        this.transferId = transferId;
    }
}