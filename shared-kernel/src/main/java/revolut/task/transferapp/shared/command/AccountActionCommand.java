package revolut.task.transferapp.shared.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import revolut.task.transferapp.command.Command;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Getter
abstract class AccountActionCommand extends Command {
    private final TransferId transferId;
    private final AccountId sourceAccountId;
    private final AccountId targetAccountId;
    private final String title;
    private final BigDecimal amount;
}
