package revolut.task.transferapp.shared;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import revolut.task.transferapp.domain.GenericId;

public final class AccountId extends GenericId {
    @JsonCreator
    public AccountId(@JsonProperty("id") String id) {
        super(id);
    }
}
