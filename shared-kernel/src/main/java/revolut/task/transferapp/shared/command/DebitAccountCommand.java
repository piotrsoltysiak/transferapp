package revolut.task.transferapp.shared.command;

import lombok.Builder;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;

import java.math.BigDecimal;

public class DebitAccountCommand extends AccountActionCommand {

    @Builder
    public DebitAccountCommand(TransferId transferId, AccountId sourceAccountId, AccountId targetAccountId, String title, BigDecimal amount) {
        super(transferId, sourceAccountId, targetAccountId, title, amount);
    }
}
