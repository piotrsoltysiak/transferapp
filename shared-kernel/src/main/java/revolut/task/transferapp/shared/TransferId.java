package revolut.task.transferapp.shared;

import com.fasterxml.jackson.annotation.JsonProperty;

import revolut.task.transferapp.domain.GenericId;

public class TransferId extends GenericId {
    public TransferId(@JsonProperty("id") String id) {
        super(id);
    }
}
