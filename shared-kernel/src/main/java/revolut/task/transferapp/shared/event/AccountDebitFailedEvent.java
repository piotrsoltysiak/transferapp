package revolut.task.transferapp.shared.event;

import lombok.Builder;
import lombok.Getter;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;

import java.time.LocalDateTime;

@Getter
public class AccountDebitFailedEvent extends DomainEvent<AccountId> {
    private final TransferId transferId;
    private final String reason;

    @Builder
    public AccountDebitFailedEvent(AccountId aggregateId, int version, LocalDateTime timestamp, TransferId transferId, String reason) {
        super(aggregateId, version, timestamp);
        this.transferId = transferId;
        this.reason = reason;
    }
}
