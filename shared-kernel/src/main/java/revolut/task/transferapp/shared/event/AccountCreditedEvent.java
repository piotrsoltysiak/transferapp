package revolut.task.transferapp.shared.event;

import lombok.Builder;
import lombok.Getter;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class AccountCreditedEvent extends DomainEvent<AccountId> {
    private final BigDecimal amount;
    private final AccountId sourceAccountId;
    private final String title;
    private final TransferId transferId;

    @Builder
    public AccountCreditedEvent(AccountId aggregateId, int version, LocalDateTime timestamp, BigDecimal amount, AccountId sourceAccountId, String title, TransferId transferId) {
        super(aggregateId, version, timestamp);
        this.amount = amount;
        this.sourceAccountId = sourceAccountId;
        this.title = title;
        this.transferId = transferId;
    }
}
