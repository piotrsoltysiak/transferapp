package revolut.task.transferapp.domain.event;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import revolut.task.transferapp.domain.GenericId;
import revolut.task.transferapp.domain.ValueObject;

import java.time.LocalDateTime;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public abstract class DomainEvent<ID extends GenericId> extends ValueObject {
    private final ID aggregateId;
    private final int version;
    private final LocalDateTime timestamp;
}
