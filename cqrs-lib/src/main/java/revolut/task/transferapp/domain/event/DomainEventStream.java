package revolut.task.transferapp.domain.event;

import java.util.List;

public interface DomainEventStream extends Iterable<DomainEvent> {
    long version();
    void addAll(List<DomainEvent> changes);
    long count();
}
