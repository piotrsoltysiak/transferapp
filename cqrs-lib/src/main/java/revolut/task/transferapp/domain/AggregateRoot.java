package revolut.task.transferapp.domain;

import lombok.Getter;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.domain.event.DomainEventStream;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AggregateRoot<T extends GenericId> {
    private T id;
    private int version = 0;
    private LocalDateTime timestamp = LocalDateTime.now();
    private final List<DomainEvent> uncommittedEvents = new ArrayList<>();

    public void loadFromHistory(DomainEventStream history) {
        history.forEach(domainEvent -> applyChange(domainEvent, false));
    }

    protected int nextVersion() {
        return version + 1;
    }

    protected void applyChange(DomainEvent event) {
        applyChange(event, true);
    }

    private void applyChange(DomainEvent event, boolean isNew) {
        updateMetadata(event);
        invokeHandlerMethod(event);
        if (isNew) uncommittedEvents.add(event);
    }

    private void updateMetadata(DomainEvent event) {
        this.id = (T) event.getAggregateId();
        this.version = event.getVersion();
        this.timestamp = event.getTimestamp();
    }

    private void invokeHandlerMethod(DomainEvent event) {
        Method handlerMethod = getHandlerMethod(event);
        if (handlerMethod != null) {
            handlerMethod.setAccessible(true);
            try {
                handlerMethod.invoke(this, event);
            } catch (Exception e) {
                throw new RuntimeException("Unable to call event handler method for " + event.getClass().getName(), e);
            }
        }
    }

    private Method getHandlerMethod(DomainEvent event) {
        try {
            return getClass().getDeclaredMethod("handleEvent", event.getClass());
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    public boolean hasUncommittedEvents() {
        return !uncommittedEvents.isEmpty();
    }

}

