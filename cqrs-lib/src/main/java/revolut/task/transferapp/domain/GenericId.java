package revolut.task.transferapp.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GenericId extends ValueObject {
    private final String id;

    @Override
    public String toString() {
        return id;
    }



}