package revolut.task.transferapp.domain.event;

import java.util.List;

public interface DomainEventBus {
    void publish(List<DomainEvent> events);
    <T extends DomainEventListener> T register(T listener);
}
