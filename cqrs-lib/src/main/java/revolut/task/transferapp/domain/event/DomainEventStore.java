package revolut.task.transferapp.domain.event;

import java.util.List;

import revolut.task.transferapp.domain.AggregateRoot;
import revolut.task.transferapp.domain.GenericId;

public interface DomainEventStore {
    DomainEventStream loadEventStream(GenericId aggregateId);
    void store(GenericId id, Class<? extends AggregateRoot> type, List<DomainEvent> events);
}