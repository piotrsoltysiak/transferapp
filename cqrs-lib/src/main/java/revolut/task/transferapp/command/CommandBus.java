package revolut.task.transferapp.command;

public interface CommandBus {

    void dispatch(Command command);

    <T extends CommandHandler> T register(T handler);

}
