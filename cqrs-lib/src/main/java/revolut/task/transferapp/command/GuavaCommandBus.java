package revolut.task.transferapp.command;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GuavaCommandBus implements CommandBus {

    private final EventBus commandBus;

    private GuavaCommandBus(EventBus commandBus) {
        this.commandBus = commandBus;
    }

    public static CommandBus asyncGuavaCommandBus(String name, Executor executor) {
        return new GuavaCommandBus(new AsyncEventBus(name, executor));
    }

    public static CommandBus syncGuavaCommandBus(String name) {
        return new GuavaCommandBus(new EventBus(name));
    }

    @Override
    public <T extends CommandHandler> T register(T handler) {
        commandBus.register(handler);
        return handler;
    }

    @Override
    public void dispatch(Command command) {
        commandBus.post(command);
    }

}
