package revolut.task.transferapp.query;

import revolut.task.transferapp.domain.GenericId;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class GenericProjectionRepository<ID extends GenericId, P extends Projection> {

    protected final Map<ID, P> projections = new ConcurrentHashMap<>();

    public void save(P projection) {
        projections.put(projection.getId(), projection);
    }

    public P getForId(ID id) {
        return projections.get(id);
    }
}
