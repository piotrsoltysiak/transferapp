package revolut.task.transferapp.query;

import revolut.task.transferapp.domain.GenericId;

public interface Projection {
    <ID extends GenericId> ID getId();
}
