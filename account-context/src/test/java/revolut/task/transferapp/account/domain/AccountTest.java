package revolut.task.transferapp.account.domain;

import org.junit.Test;
import revolut.task.transferapp.account.domain.event.AccountCreatedEvent;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.shared.event.AccountCreditedEvent;
import revolut.task.transferapp.shared.event.AccountDebitedEvent;
import revolut.task.transferapp.shared.event.AccountDebitFailedEvent;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    @Test
    public void should_create_account() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());

        // When
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(1_000), "Jan", "Nowak", "123456790ID");

        // Then
        assertThat(account).isNotNull();
        assertThat(account.getId()).isNotNull();
        assertThat(account.getUncommittedEvents()).hasSize(1);
        assertThat(account.getUncommittedEvents().get(0)).isOfAnyClassIn(AccountCreatedEvent.class);
        AccountCreatedEvent event = (AccountCreatedEvent) account.getUncommittedEvents().get(0);
        assertThat(event.getCurrency()).isEqualByComparingTo(Currency.PLN);
        assertThat(event.getStartBalance()).isEqualByComparingTo(BigDecimal.valueOf(1_000));
        assertThat(event.getOwnerFirstName()).isEqualTo("Jan");
        assertThat(event.getOwnerLastName()).isEqualTo("Nowak");
        assertThat(event.getOwnerIdNumber()).isEqualTo("123456790ID");
        assertThat(event.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
        assertThat(event.getVersion()).isEqualTo(0);
    }

    @Test
    public void should_debit_account() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(1_000), "Jan", "Nowak", "123456790ID");
        AccountId targetAccountId  = new AccountId(UUID.randomUUID().toString());
        TransferId transferId = new TransferId(UUID.randomUUID().toString());

        // When
        account.debit(targetAccountId, BigDecimal.valueOf(100), "test transfer", transferId);

        // Then
        assertThat(account.getUncommittedEvents()).hasSize(2);
        assertThat(account.getUncommittedEvents().get(0)).isInstanceOf(AccountCreatedEvent.class);
        AccountCreatedEvent accountCreatedEvent = (AccountCreatedEvent) account.getUncommittedEvents().get(0);
        assertThat(account.getUncommittedEvents().get(1)).isInstanceOf(AccountDebitedEvent.class);
        AccountDebitedEvent accountDebitedEvent = (AccountDebitedEvent) account.getUncommittedEvents().get(1);
        assertThat(accountDebitedEvent.getAggregateId()).isEqualTo(accountId);
        assertThat(accountDebitedEvent.getAmount()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(accountDebitedEvent.getTargetAccountId()).isEqualTo(targetAccountId);
        assertThat(accountDebitedEvent.getTitle()).isEqualTo("test transfer");
        assertThat(accountDebitedEvent.getTransferId()).isEqualTo(transferId);
        assertThat(accountDebitedEvent.getVersion()).isEqualTo(1);
        assertThat(accountDebitedEvent.getTimestamp()).isAfterOrEqualTo(accountCreatedEvent.getTimestamp());
        assertThat(accountDebitedEvent.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
    }

    @Test
    public void should_not_debit_account() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(0), "Jan", "Nowak", "123456790ID");
        AccountId targetAccountId  = new AccountId(UUID.randomUUID().toString());
        TransferId transferId = new TransferId(UUID.randomUUID().toString());

        // When
        account.debit(targetAccountId, BigDecimal.valueOf(100), "test transfer", transferId);

        // Then
        assertThat(account.getUncommittedEvents()).hasSize(2);
        assertThat(account.getUncommittedEvents().get(0)).isInstanceOf(AccountCreatedEvent.class);
        AccountCreatedEvent accountCreatedEvent = (AccountCreatedEvent) account.getUncommittedEvents().get(0);
        assertThat(account.getUncommittedEvents().get(1)).isInstanceOf(AccountDebitFailedEvent.class);
        AccountDebitFailedEvent accountDebitedEvent = (AccountDebitFailedEvent) account.getUncommittedEvents().get(1);
        assertThat(accountDebitedEvent.getAggregateId()).isEqualTo(accountId);
        assertThat(accountDebitedEvent.getTransferId()).isEqualTo(transferId);
        assertThat(accountDebitedEvent.getVersion()).isEqualTo(1);
        assertThat(accountDebitedEvent.getTimestamp()).isAfterOrEqualTo(accountCreatedEvent.getTimestamp());
        assertThat(accountDebitedEvent.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
    }

    @Test
    public void should_credit_account() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(0), "Jan", "Nowak", "123456790ID");
        AccountId sourceAccountId  = new AccountId(UUID.randomUUID().toString());
        TransferId transferId = new TransferId(UUID.randomUUID().toString());

        // When
        account.credit(sourceAccountId, BigDecimal.valueOf(100), "test transfer", transferId);

        // Then
        assertThat(account.getUncommittedEvents()).hasSize(2);
        assertThat(account.getUncommittedEvents().get(0)).isInstanceOf(AccountCreatedEvent.class);
        AccountCreatedEvent accountCreatedEvent = (AccountCreatedEvent) account.getUncommittedEvents().get(0);
        assertThat(account.getUncommittedEvents().get(1)).isInstanceOf(AccountCreditedEvent.class);
        AccountCreditedEvent accountDebitedEvent = (AccountCreditedEvent) account.getUncommittedEvents().get(1);
        assertThat(accountDebitedEvent.getAggregateId()).isEqualTo(accountId);
        assertThat(accountDebitedEvent.getTransferId()).isEqualTo(transferId);
        assertThat(accountDebitedEvent.getSourceAccountId()).isEqualTo(sourceAccountId);
        assertThat(accountDebitedEvent.getAmount()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(accountDebitedEvent.getTitle()).isEqualTo("test transfer");
        assertThat(accountDebitedEvent.getVersion()).isEqualTo(1);
        assertThat(accountDebitedEvent.getTimestamp()).isAfterOrEqualTo(accountCreatedEvent.getTimestamp());
        assertThat(accountDebitedEvent.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
    }

    @Test
    public void should_handle_account_created_event() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(1_000), "Jan", "Nowak", "123456790ID");
        AccountCreatedEvent accountCreatedEvent = (AccountCreatedEvent) account.getUncommittedEvents().get(0);

        // When
        account.handleEvent(accountCreatedEvent);

        // Then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(1_000));
    }

    @Test
    public void should_handle_account_debited_event() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(1_000), "Jan", "Nowak", "123456790ID");
        AccountCreatedEvent accountCreatedEvent = (AccountCreatedEvent) account.getUncommittedEvents().get(0);
        AccountDebitedEvent accountDebitedEvent = AccountDebitedEvent.builder()
                .amount(BigDecimal.valueOf(100))
                .build();
        account.handleEvent(accountCreatedEvent);

        // When
        account.handleEvent(accountDebitedEvent);

        // Then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(900));
    }

    @Test
    public void should_handle_account_credited_event() {
        // Given
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, Currency.PLN, BigDecimal.valueOf(0), "Jan", "Nowak", "123456790ID");
        AccountCreatedEvent accountCreatedEvent = (AccountCreatedEvent) account.getUncommittedEvents().get(0);
        AccountCreditedEvent accountCreditedEvent = AccountCreditedEvent.builder()
                .amount(BigDecimal.valueOf(100))
                .build();
        account.handleEvent(accountCreatedEvent);

        // When
        account.handleEvent(accountCreditedEvent);

        // Then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(100));
    }
}
