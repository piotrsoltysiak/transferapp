package revolut.task.transferapp.account.query;

import lombok.Data;

@Data
public class AccountOwnerProjection {
    private String firstName;
    private String lastName;
    private String idNumber;
}
