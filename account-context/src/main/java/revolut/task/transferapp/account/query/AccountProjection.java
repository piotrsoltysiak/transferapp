package revolut.task.transferapp.account.query;

import lombok.Data;
import revolut.task.transferapp.account.domain.Currency;
import revolut.task.transferapp.query.Projection;
import revolut.task.transferapp.shared.AccountId;

import java.math.BigDecimal;

@Data
public class AccountProjection implements Projection {
    private AccountId id;
    private AccountOwnerProjection accountOwner;
    private Currency currency;
    private BigDecimal balance;
}
