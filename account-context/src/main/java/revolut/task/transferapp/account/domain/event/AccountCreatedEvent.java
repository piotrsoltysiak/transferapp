package revolut.task.transferapp.account.domain.event;

import lombok.Builder;
import lombok.Getter;
import revolut.task.transferapp.account.domain.Currency;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.AccountId;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class AccountCreatedEvent extends DomainEvent<AccountId> {
    private final Currency currency;
    private final BigDecimal startBalance;
    private final String ownerFirstName;
    private final String ownerLastName;
    private final String ownerIdNumber;

    @Builder
    public AccountCreatedEvent(AccountId aggregateId, int version, LocalDateTime timestamp, Currency currency, BigDecimal startBalance, String ownerFirstName, String ownerLastName, String ownerIdNumber) {
        super(aggregateId, version, timestamp);
        this.currency = currency;
        this.startBalance = startBalance;
        this.ownerFirstName = ownerFirstName;
        this.ownerLastName = ownerLastName;
        this.ownerIdNumber = ownerIdNumber;
    }
}
