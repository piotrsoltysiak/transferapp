package revolut.task.transferapp.account.query;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.shared.AccountId;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/accounts")
@RequiredArgsConstructor
@Slf4j
public class AccountResource {
    private final AccountProjectionRepository repository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<AccountProjection> getAccountListForOwnerIdNumber(@QueryParam("ownerIdNumber") String ownerIdNumber) {
        log.debug("Getting account list for ownerId {}", ownerIdNumber);
        return repository.getAccountListForOwnerIdNumber(ownerIdNumber);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{accountId}")
    public AccountProjection getAccountForId(@PathParam("accountId") String accountId) {
        log.debug("Getting account for id {}", accountId);
        return repository.getForId(new AccountId(accountId));
    }

}
