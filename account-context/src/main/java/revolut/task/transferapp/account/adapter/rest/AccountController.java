package revolut.task.transferapp.account.adapter.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.account.command.CreateAccountCommand;
import revolut.task.transferapp.command.CommandBus;

@Path("/accounts")
@RequiredArgsConstructor
@Slf4j
public class AccountController {
    private final CommandBus commandBus;

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(CreateAccountCommand command) {
        log.info("Received create account request: {}", command);
        commandBus.dispatch(command);
        return Response.ok().build();
    }

}
