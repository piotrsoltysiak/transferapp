package revolut.task.transferapp.account.domain;

import revolut.task.transferapp.account.domain.event.AccountCreatedEvent;
import revolut.task.transferapp.domain.AggregateRoot;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.shared.event.AccountCreditedEvent;
import revolut.task.transferapp.shared.event.AccountDebitedEvent;
import revolut.task.transferapp.shared.event.AccountDebitFailedEvent;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public final class Account extends AggregateRoot<AccountId> {
    private BigDecimal balance = BigDecimal.ZERO;

    public static Account create(AccountId accountId, Currency accountCurrency, BigDecimal accountStartBalance, String ownerFirstName,
                                 String ownerLastName, String ownerIdNumber) {
        Account account = new Account();
        AccountCreatedEvent accountCreatedEvent = AccountCreatedEvent.builder()
                .aggregateId(accountId)
                .currency(accountCurrency)
                .ownerFirstName(ownerFirstName)
                .ownerLastName(ownerLastName)
                .ownerIdNumber(ownerIdNumber)
                .startBalance(accountStartBalance)
                .version(0)
                .timestamp(LocalDateTime.now())
                .build();

        account.applyChange(accountCreatedEvent);
        return account;
    }

    public void debit(AccountId targetAccountId, BigDecimal amount, String title, TransferId transferId) {
        if (balance.compareTo(amount) < 0) {
            AccountDebitFailedEvent accountDebitFailedEvent = AccountDebitFailedEvent.builder()
                    .aggregateId(this.getId())
                    .reason("Not enough money")
                    .timestamp(LocalDateTime.now())
                    .transferId(transferId)
                    .version(nextVersion())
                    .build();
            applyChange(accountDebitFailedEvent);

        } else {
            AccountDebitedEvent accountDebitedEvent = AccountDebitedEvent.builder()
                    .aggregateId(this.getId())
                    .amount(amount)
                    .targetAccountId(targetAccountId)
                    .timestamp(LocalDateTime.now())
                    .title(title)
                    .transferId(transferId)
                    .version(nextVersion())
                    .build();

            applyChange(accountDebitedEvent);
        }
    }

    public void credit(AccountId sourceAccountId, BigDecimal amount, String title, TransferId transferId) {
        AccountCreditedEvent accountCreditedEvent = AccountCreditedEvent.builder()
                .aggregateId(this.getId())
                .amount(amount)
                .sourceAccountId(sourceAccountId)
                .timestamp(LocalDateTime.now())
                .transferId(transferId)
                .version(nextVersion())
                .title(title)
                .build();
        applyChange(accountCreditedEvent);
    }

    public BigDecimal balance() {
        return balance;
    }

    void handleEvent(AccountCreatedEvent event) {
        this.balance = event.getStartBalance();
    }

    void handleEvent(AccountDebitedEvent event) {
        this.balance = this.balance.subtract(event.getAmount());
    }

    void handleEvent(AccountCreditedEvent event) {
        this.balance = this.balance.add(event.getAmount());
    }


}
