package revolut.task.transferapp.account.domain;

public enum Currency {
    EUR, GBP, PLN, USD;
}
