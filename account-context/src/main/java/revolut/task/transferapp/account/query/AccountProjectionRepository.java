package revolut.task.transferapp.account.query;


import revolut.task.transferapp.query.GenericProjectionRepository;
import revolut.task.transferapp.shared.AccountId;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AccountProjectionRepository extends GenericProjectionRepository<AccountId, AccountProjection> {
    List<AccountProjection> getAccountListForOwnerIdNumber(String ownerIdNumber) {
        return projections.entrySet().stream()
                .filter(entry -> ownerIdNumber.equals(entry.getValue().getAccountOwner().getIdNumber()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
}
