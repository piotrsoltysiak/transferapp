package revolut.task.transferapp.account.command;

import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.account.domain.Account;
import revolut.task.transferapp.command.CommandHandler;
import revolut.task.transferapp.domain.Repository;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.command.CreditAccountCommand;
import revolut.task.transferapp.shared.command.DebitAccountCommand;

import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
public class AccountCommandHandler implements CommandHandler {
    private final Repository accountRepository;

    @Subscribe
    public void handle(CreateAccountCommand command) {
        AccountId accountId = new AccountId(UUID.randomUUID().toString());
        Account account = Account.create(accountId, command.getAccountCurrency(), command.getAccountStartBalance(),
                command.getOwnerFirstName(), command.getOwnerLastName(), command.getOwnerIdNumber());
        accountRepository.save(account);
        log.debug("Command {} successfully handled", command);
    }

    @Subscribe
    public void handle(DebitAccountCommand command) {
        Account account = accountRepository.load(command.getSourceAccountId(), Account.class);
        account.debit(command.getTargetAccountId(), command.getAmount(), command.getTitle(), command.getTransferId());
        accountRepository.save(account);
        log.debug("Command {} successfully handled", command);
    }

    @Subscribe
    public void handle(CreditAccountCommand command) {
        Account account = accountRepository.load(command.getTargetAccountId(), Account.class);
        account.credit(command.getTargetAccountId(), command.getAmount(), command.getTitle(), command.getTransferId());
        accountRepository.save(account);
        log.debug("Command {} successfully handled", command);
    }
}
