package revolut.task.transferapp.account.config;

import lombok.RequiredArgsConstructor;
import revolut.task.transferapp.account.adapter.rest.AccountController;
import revolut.task.transferapp.account.command.AccountCommandHandler;
import revolut.task.transferapp.account.query.AccountListDenormalizer;
import revolut.task.transferapp.account.query.AccountProjectionRepository;
import revolut.task.transferapp.account.query.AccountResource;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.command.CommandHandler;
import revolut.task.transferapp.domain.Repository;

@RequiredArgsConstructor
public class AccountContextConfig {
    private final CommandBus commandBus;
    private final Repository repository;
    private final AccountProjectionRepository accountProjectionRepository = new AccountProjectionRepository();

    public AccountController accountController() {
        return new AccountController(commandBus);
    }

    public AccountResource accountResource() {
        return new AccountResource(accountProjectionRepository);
    }

    public AccountListDenormalizer accountListDenormalizer() {
        return new AccountListDenormalizer(accountProjectionRepository);
    }

    public CommandHandler accountCommandHandler() {
        return new AccountCommandHandler(repository);
    }
}
