package revolut.task.transferapp.account.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import revolut.task.transferapp.account.domain.Currency;
import revolut.task.transferapp.command.Command;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class CreateAccountCommand extends Command {
    private String ownerFirstName;
    private String ownerLastName;
    private String ownerIdNumber;
    private Currency accountCurrency;
    private BigDecimal accountStartBalance;
}
