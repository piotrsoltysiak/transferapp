package revolut.task.transferapp.account.query;

import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.account.domain.event.AccountCreatedEvent;
import revolut.task.transferapp.domain.event.DomainEventListener;
import revolut.task.transferapp.shared.event.AccountCreditedEvent;
import revolut.task.transferapp.shared.event.AccountDebitedEvent;

@Slf4j
@RequiredArgsConstructor
public class AccountListDenormalizer implements DomainEventListener {
    private final AccountProjectionRepository repository;

    @Subscribe
    public void handleEvent(AccountCreatedEvent event) {
        log.debug("Handling event {}", event);
        AccountOwnerProjection accountOwner = new AccountOwnerProjection();
        accountOwner.setFirstName(event.getOwnerFirstName());
        accountOwner.setLastName(event.getOwnerLastName());
        accountOwner.setIdNumber(event.getOwnerIdNumber());

        AccountProjection accountProjection = new AccountProjection();
        accountProjection.setBalance(event.getStartBalance());
        accountProjection.setCurrency(event.getCurrency());
        accountProjection.setId(event.getAggregateId());
        accountProjection.setAccountOwner(accountOwner);
        repository.save(accountProjection);
    }

    @Subscribe
    public void handleEvent(AccountDebitedEvent event) {
        log.debug("Handling event {}", event);
        AccountProjection accountProjection = repository.getForId(event.getAggregateId());
        accountProjection.setBalance(accountProjection.getBalance().subtract(event.getAmount()));
        repository.save(accountProjection);
    }

    @Subscribe
    public void handleEvent(AccountCreditedEvent event) {
        log.debug("Handling event {}", event);
        AccountProjection accountProjection = repository.getForId(event.getAggregateId());
        accountProjection.setBalance(accountProjection.getBalance().add(event.getAmount()));
        repository.save(accountProjection);
    }
}

