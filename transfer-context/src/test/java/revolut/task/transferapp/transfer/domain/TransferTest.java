package revolut.task.transferapp.transfer.domain;

import org.junit.Test;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.transfer.domain.event.TransferFailedEvent;
import revolut.task.transferapp.transfer.domain.event.TransferFinishedEvent;
import revolut.task.transferapp.transfer.domain.event.TransferStartedEvent;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferTest {

    @Test
    public void should_begin_transfer() {
        // Given
        TransferId transferId = new TransferId(UUID.randomUUID().toString());
        AccountId sourceAccountId = new AccountId(UUID.randomUUID().toString());
        AccountId targetAccountId = new AccountId(UUID.randomUUID().toString());
        BigDecimal amount = BigDecimal.valueOf(100);
        String title = "test-transfer";
        Transfer transfer = new Transfer();

        // When
        transfer.begin(transferId, sourceAccountId, targetAccountId, amount, title);

        // Then
        assertThat(transfer.getUncommittedEvents()).hasSize(1);
        assertThat(transfer.getUncommittedEvents().get(0)).isInstanceOf(TransferStartedEvent.class);
        TransferStartedEvent transferStartedEvent = (TransferStartedEvent) transfer.getUncommittedEvents().get(0);
        assertThat(transferStartedEvent.getAmount()).isEqualTo(amount);
        assertThat(transferStartedEvent.getTitle()).isEqualTo(title);
        assertThat(transferStartedEvent.getSourceAccountId()).isEqualTo(sourceAccountId);
        assertThat(transferStartedEvent.getTargetAccountId()).isEqualTo(targetAccountId);
        assertThat(transferStartedEvent.getAggregateId()).isEqualTo(transferId);
        assertThat(transferStartedEvent.getVersion()).isEqualTo(0);
        assertThat(transferStartedEvent.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
    }

    @Test
    public void should_fail_transfer() {
        // Given
        Transfer transfer = getStartedTransfer();
        String reason = "test reason";

        // When
        transfer.fail(reason);

        // Then
        assertThat(transfer.getUncommittedEvents()).hasSize(2);
        assertThat(transfer.getUncommittedEvents().get(1)).isInstanceOf(TransferFailedEvent.class);
        TransferStartedEvent transferStartedEvent = (TransferStartedEvent) transfer.getUncommittedEvents().get(0);
        TransferFailedEvent transferFailedEvent = (TransferFailedEvent) transfer.getUncommittedEvents().get(1);

        assertThat(transferFailedEvent.getReason()).isEqualTo(reason);
        assertThat(transferFailedEvent.getAggregateId()).isEqualTo(transfer.getId());
        assertThat(transferFailedEvent.getVersion()).isEqualTo(1);
        assertThat(transferFailedEvent.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
        assertThat(transferFailedEvent.getTimestamp()).isAfterOrEqualTo(transferStartedEvent.getTimestamp());
    }

    @Test
    public void should_finish_transfer() {
        // Given
        Transfer transfer = getStartedTransfer();

        // When
        transfer.finish();

        // Then
        assertThat(transfer.getUncommittedEvents()).hasSize(2);
        assertThat(transfer.getUncommittedEvents().get(1)).isInstanceOf(TransferFinishedEvent.class);
        TransferStartedEvent transferStartedEvent = (TransferStartedEvent) transfer.getUncommittedEvents().get(0);
        TransferFinishedEvent transferFinishedEvent = (TransferFinishedEvent) transfer.getUncommittedEvents().get(1);

        assertThat(transferFinishedEvent.getAggregateId()).isEqualTo(transfer.getId());
        assertThat(transferFinishedEvent.getVersion()).isEqualTo(1);
        assertThat(transferFinishedEvent.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
        assertThat(transferFinishedEvent.getTimestamp()).isAfterOrEqualTo(transferStartedEvent.getTimestamp());
    }

    @Test
    public void should_set_state_to_pending_after_transfer_started_event() {
        // Given
        Transfer transfer = getStartedTransfer();
        TransferStartedEvent transferStartedEvent = (TransferStartedEvent) transfer.getUncommittedEvents().get(0);

        // When
        transfer.handleEvent(transferStartedEvent);

        // Then
        assertThat(transfer.isPending());
    }

    @Test
    public void should_set_state_to_failed_after_transfer_failed_event() {
        // Given
        Transfer transfer = getStartedTransfer();
        TransferFailedEvent transferFailedEvent = TransferFailedEvent.builder().build();

        // When
        transfer.handleEvent(transferFailedEvent);

        // Then
        assertThat(transfer.isFailed());
    }

    @Test
    public void should_set_state_to_finished_after_transfer_finished_event() {
        // Given
        Transfer transfer = getStartedTransfer();
        TransferFinishedEvent transferFinishedEvent = TransferFinishedEvent.builder().build();

        // When
        transfer.handleEvent(transferFinishedEvent);

        // Then
        assertThat(transfer.isFinished());
    }

    private Transfer getStartedTransfer() {
        TransferId transferId = new TransferId(UUID.randomUUID().toString());
        AccountId sourceAccountId = new AccountId(UUID.randomUUID().toString());
        AccountId targetAccountId = new AccountId(UUID.randomUUID().toString());
        BigDecimal amount = BigDecimal.valueOf(100);
        String title = "test-transfer";
        Transfer transfer = new Transfer();
        transfer.begin(transferId, sourceAccountId, targetAccountId, amount, title);

        return transfer;
    }
}