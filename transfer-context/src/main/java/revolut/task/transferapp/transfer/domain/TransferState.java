package revolut.task.transferapp.transfer.domain;

public enum  TransferState {
    PENDING, FINISHED, FAILED;
}
