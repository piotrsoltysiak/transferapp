package revolut.task.transferapp.transfer.domain;

import revolut.task.transferapp.domain.AggregateRoot;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.transfer.domain.event.TransferFailedEvent;
import revolut.task.transferapp.transfer.domain.event.TransferFinishedEvent;
import revolut.task.transferapp.transfer.domain.event.TransferStartedEvent;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Transfer extends AggregateRoot<TransferId> {
    private TransferState state;

    public void begin(TransferId transferId, AccountId sourceAccountId, AccountId targetAccountId, BigDecimal amount, String title) {
        applyChange(TransferStartedEvent.builder()
                .aggregateId(transferId)
                .amount(amount)
                .sourceAccountId(sourceAccountId)
                .targetAccountId(targetAccountId)
                .title(title)
                .timestamp(LocalDateTime.now())
                .version(0)
                .build());
    }

    public void fail(String reason) {
        if (isPending()) {
            applyChange(TransferFailedEvent.builder()
                    .aggregateId(this.getId())
                    .reason(reason)
                    .timestamp(LocalDateTime.now())
                    .version(nextVersion())
                    .build());
        }
    }

    public void finish() {
        if (isPending()) {
            applyChange(TransferFinishedEvent.builder()
                    .aggregateId(this.getId())
                    .timestamp(LocalDateTime.now())
                    .version(nextVersion())
                    .build());
        }
    }

    boolean isPending() {
        return state == TransferState.PENDING;
    }

    boolean isFailed() {
        return state == TransferState.FAILED;
    }

    boolean isFinished() {
        return state == TransferState.FINISHED;
    }

    void handleEvent(TransferStartedEvent event) {
        this.state = TransferState.PENDING;
    }

    void handleEvent(TransferFailedEvent event) {
        this.state = TransferState.FAILED;
    }

    void handleEvent(TransferFinishedEvent event) {
        this.state = TransferState.FINISHED;
    }
}