package revolut.task.transferapp.transfer.query;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.shared.TransferId;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/transfers")
@RequiredArgsConstructor
@Slf4j
public class TransferResource {
    private final TransferProjectionRepository repository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransferProjection> getTransferListForAccountId(@QueryParam("accountId") String accountId) {
        log.debug("Getting transfer list for accountId {}", accountId);
        return repository.getTransferListForAccountId(accountId);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{transferId}")
    public TransferProjection getTransferForId(@PathParam("transferId") String transferId) {
        log.debug("Getting transfer for id {}", transferId);
        return repository.getForId(new TransferId(transferId));
    }

}
