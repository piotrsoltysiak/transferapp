package revolut.task.transferapp.transfer.query;

import lombok.Data;
import revolut.task.transferapp.query.Projection;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.transfer.domain.TransferState;

import java.math.BigDecimal;

@Data
public class TransferProjection implements Projection {
    private AccountId fromAccount;
    private AccountId toAccount;
    private TransferId id;
    private BigDecimal amount;
    private String title;
    private TransferState state;
    private String failureReason;
}
