package revolut.task.transferapp.transfer.application.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import revolut.task.transferapp.command.Command;
import revolut.task.transferapp.shared.AccountId;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
public class BeginTransferCommand extends Command {
    private AccountId sourceAccountId;
    private AccountId targetAccountId;
    private BigDecimal amount;
    private String title;
}
