package revolut.task.transferapp.transfer.adapter.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.RequiredArgsConstructor;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.transfer.application.command.BeginTransferCommand;

@Path("/transfers")
@RequiredArgsConstructor
public class TransferController {
    private final CommandBus commandBus;

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transferMoney(BeginTransferCommand command) {
        commandBus.dispatch(command);
        return Response.ok().build();
    }
}
