package revolut.task.transferapp.transfer.domain.event;

import lombok.Builder;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.TransferId;

import java.time.LocalDateTime;

public class TransferFinishedEvent extends DomainEvent<TransferId> {

    @Builder
    protected TransferFinishedEvent(TransferId aggregateId, int version, LocalDateTime timestamp) {
        super(aggregateId, version, timestamp);
    }
}
