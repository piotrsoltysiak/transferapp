package revolut.task.transferapp.transfer.application;

import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.command.CommandHandler;
import revolut.task.transferapp.domain.Repository;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.transfer.application.command.BeginTransferCommand;
import revolut.task.transferapp.transfer.application.command.FailTransferCommand;
import revolut.task.transferapp.transfer.application.command.FinishTransferCommand;
import revolut.task.transferapp.transfer.domain.Transfer;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class TransferCommandHandler implements CommandHandler {
    private final Repository repository;

    @Subscribe
    public void handle(BeginTransferCommand command) {
        log.debug("Handling command {}", command);
        Transfer transfer = new Transfer();
        TransferId transferId = new TransferId(UUID.randomUUID().toString());
        transfer.begin(transferId, command.getSourceAccountId(), command.getTargetAccountId(), command.getAmount(), command.getTitle());
        repository.save(transfer);
    }

    @Subscribe
    public void handle(FailTransferCommand command) {
        log.debug("Handling command {}", command);
        Transfer transfer = repository.load(command.getTransferId(), Transfer.class);
        transfer.fail(command.getReason());
        repository.save(transfer);
    }

    @Subscribe
    public void handle(FinishTransferCommand command) {
        log.debug("Handling command {}", command);
        Transfer transfer = repository.load(command.getTransferId(), Transfer.class);
        transfer.finish();
        repository.save(transfer);
    }



}
