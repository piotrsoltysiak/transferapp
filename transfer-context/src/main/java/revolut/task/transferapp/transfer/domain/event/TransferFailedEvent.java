package revolut.task.transferapp.transfer.domain.event;

import lombok.Builder;
import lombok.Getter;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.TransferId;

import java.time.LocalDateTime;

@Getter
public class TransferFailedEvent extends DomainEvent<TransferId> {
    private final String reason;

    @Builder
    public TransferFailedEvent(TransferId aggregateId, int version, LocalDateTime timestamp, String reason) {
        super(aggregateId, version, timestamp);
        this.reason = reason;
    }
}
