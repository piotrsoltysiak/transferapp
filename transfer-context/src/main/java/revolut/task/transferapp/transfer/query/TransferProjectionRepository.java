package revolut.task.transferapp.transfer.query;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import revolut.task.transferapp.query.GenericProjectionRepository;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;
import revolut.task.transferapp.transfer.domain.TransferState;

public class TransferProjectionRepository extends GenericProjectionRepository<TransferId, TransferProjection> {

    List<TransferProjection> getTransferListForAccountId(String accountIdNumber) {
        AccountId accountId = new AccountId(accountIdNumber);

        return projections.entrySet().stream()
                .filter(getTransferFilterPredicate(accountId))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private Predicate<Map.Entry<TransferId, TransferProjection>> getTransferFilterPredicate(AccountId accountId) {
        return transfer -> isFromAccount(accountId, transfer) || (isToAccount(accountId, transfer) && isNotFailed(transfer));
    }

    private boolean isNotFailed(Map.Entry<TransferId, TransferProjection> entry) {
        return !entry.getValue().getState().equals(TransferState.FAILED);
    }

    private boolean isToAccount(AccountId accountId, Map.Entry<TransferId, TransferProjection> entry) {
        return accountId.equals(entry.getValue().getToAccount());
    }

    private boolean isFromAccount(AccountId accountId, Map.Entry<TransferId, TransferProjection> entry) {
        return accountId.equals(entry.getValue().getFromAccount());
    }


}
