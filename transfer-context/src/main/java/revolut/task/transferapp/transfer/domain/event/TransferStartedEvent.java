package revolut.task.transferapp.transfer.domain.event;


import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import revolut.task.transferapp.domain.event.DomainEvent;
import revolut.task.transferapp.shared.AccountId;
import revolut.task.transferapp.shared.TransferId;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@ToString(callSuper = true)
public class TransferStartedEvent extends DomainEvent<TransferId> {
    private final AccountId sourceAccountId;
    private final AccountId targetAccountId;
    private final BigDecimal amount;
    private final String title;

    @Builder
    public TransferStartedEvent(TransferId aggregateId, int version, LocalDateTime timestamp, AccountId sourceAccountId,
                                AccountId targetAccountId, BigDecimal amount, String title) {
        super(aggregateId, version, timestamp);
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.amount = amount;
        this.title = title;
    }
}
