package revolut.task.transferapp.transfer.application;

import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.domain.event.DomainEventListener;
import revolut.task.transferapp.shared.command.CreditAccountCommand;
import revolut.task.transferapp.shared.command.DebitAccountCommand;
import revolut.task.transferapp.shared.event.AccountCreditedEvent;
import revolut.task.transferapp.shared.event.AccountDebitedEvent;
import revolut.task.transferapp.shared.event.AccountDebitFailedEvent;
import revolut.task.transferapp.transfer.application.command.FailTransferCommand;
import revolut.task.transferapp.transfer.application.command.FinishTransferCommand;
import revolut.task.transferapp.transfer.domain.event.TransferStartedEvent;

@Slf4j
@RequiredArgsConstructor
public class TransferSaga implements DomainEventListener {
    private final CommandBus commandBus;

    @Subscribe
    public void handleEvent(TransferStartedEvent event) {
        log.debug("Handling event {}", event);
        DebitAccountCommand command = DebitAccountCommand.builder()
                .amount(event.getAmount())
                .sourceAccountId(event.getSourceAccountId())
                .targetAccountId(event.getTargetAccountId())
                .title(event.getTitle())
                .transferId(event.getAggregateId())
                .build();
        commandBus.dispatch(command);
    }

    @Subscribe
    public void handleEvent(AccountDebitedEvent event) {
        log.debug("Handling event {}", event);
        CreditAccountCommand command = CreditAccountCommand.builder()
                .amount(event.getAmount())
                .sourceAccountId(event.getAggregateId())
                .targetAccountId(event.getTargetAccountId())
                .title(event.getTitle())
                .transferId(event.getTransferId())
                .build();
        commandBus.dispatch(command);
    }

    @Subscribe
    public void handleEvent(AccountDebitFailedEvent event) {
        log.debug("Handling event {}", event);
        FailTransferCommand failTransferCommand = new FailTransferCommand(event.getTransferId(), event.getReason());
        commandBus.dispatch(failTransferCommand);
    }

    @Subscribe
    public void handleEvent(AccountCreditedEvent event) {
        log.debug("Handling event {}", event);
        FinishTransferCommand finishTransferCommand = new FinishTransferCommand(event.getTransferId());
        commandBus.dispatch(finishTransferCommand);

    }
}
