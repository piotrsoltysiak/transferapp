package revolut.task.transferapp.transfer.application.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import revolut.task.transferapp.command.Command;
import revolut.task.transferapp.shared.TransferId;

@Getter
@RequiredArgsConstructor
public class FinishTransferCommand extends Command {
    private final TransferId transferId;
}
