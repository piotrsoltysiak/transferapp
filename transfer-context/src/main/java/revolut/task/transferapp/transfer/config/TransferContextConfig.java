package revolut.task.transferapp.transfer.config;

import lombok.RequiredArgsConstructor;
import revolut.task.transferapp.command.CommandBus;
import revolut.task.transferapp.domain.Repository;
import revolut.task.transferapp.transfer.adapter.rest.TransferController;
import revolut.task.transferapp.transfer.application.TransferCommandHandler;
import revolut.task.transferapp.transfer.application.TransferSaga;
import revolut.task.transferapp.transfer.query.TransferListDenormalizer;
import revolut.task.transferapp.transfer.query.TransferProjectionRepository;
import revolut.task.transferapp.transfer.query.TransferResource;

@RequiredArgsConstructor
public class TransferContextConfig {
    private final CommandBus commandBus;
    private final Repository repository;
    private final TransferProjectionRepository transferProjectionRepository = new TransferProjectionRepository();

    public TransferController transferController() {
        return new TransferController(commandBus);
    }

    public TransferSaga transferSaga() {
        return new TransferSaga(commandBus);
    }

    public TransferCommandHandler transferCommandHandler() {
        return new TransferCommandHandler(repository);
    }

    public TransferListDenormalizer transferListDenormalizer() {
        return new TransferListDenormalizer(transferProjectionRepository);
    }

    public TransferResource transferResource() {
        return new TransferResource(transferProjectionRepository);
    }
}
