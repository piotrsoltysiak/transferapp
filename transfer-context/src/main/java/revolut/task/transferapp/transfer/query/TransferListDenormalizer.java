package revolut.task.transferapp.transfer.query;

import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import revolut.task.transferapp.domain.event.DomainEventListener;
import revolut.task.transferapp.transfer.domain.TransferState;
import revolut.task.transferapp.transfer.domain.event.TransferFailedEvent;
import revolut.task.transferapp.transfer.domain.event.TransferFinishedEvent;
import revolut.task.transferapp.transfer.domain.event.TransferStartedEvent;

@Slf4j
@RequiredArgsConstructor
public class TransferListDenormalizer implements DomainEventListener {
    private final TransferProjectionRepository repository;

    @Subscribe
    public void handleEvent(TransferStartedEvent event) {
        log.debug("Handling event {}", event);
        TransferProjection transferProjection = new TransferProjection();
        transferProjection.setAmount(event.getAmount());
        transferProjection.setId(event.getAggregateId());
        transferProjection.setTitle(event.getTitle());
        transferProjection.setState(TransferState.PENDING);
        transferProjection.setAmount(event.getAmount());
        transferProjection.setFromAccount(event.getSourceAccountId());
        transferProjection.setToAccount(event.getTargetAccountId());
        repository.save(transferProjection);
    }

    @Subscribe
    public void handleEvent(TransferFailedEvent event) {
        log.debug("Handling event {}", event);
        TransferProjection transferProjection = repository.getForId(event.getAggregateId());
        transferProjection.setState(TransferState.FAILED);
        transferProjection.setFailureReason(event.getReason());
        repository.save(transferProjection);
    }

    @Subscribe
    public void handleEvent(TransferFinishedEvent event) {
        log.debug("Handling event {}", event);
        TransferProjection transferProjection = repository.getForId(event.getAggregateId());
        transferProjection.setState(TransferState.FINISHED);
        repository.save(transferProjection);
    }
}
